package com.mcs.common;

public class MCSStoreFileException extends Exception {
	   private String message;

	    public MCSStoreFileException(){
	    	super();
	    }
	 	public MCSStoreFileException(String message){
	 		super(message);
	 	}
	 	public MCSStoreFileException(String message, Throwable cause){
	 		super(message,cause);
	 	}
	 	public MCSStoreFileException(Throwable cause){
	 		super(cause);
	 		
	 	}
		
		public String getMessage() {
			return message;
		}
	
		public void setMessage(String message) {
			this.message = message;
		}
}
