package com.mcs.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.HashSet;
import java.util.Set;

import model.Vuser;
import model.Vuserdata;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
 
public class MCSClient_CC  {
	 private LoadScenario loadScenario;
     private Vuser vuser;
	
    String URL =  "http://<host>:<port>/core/DummyAPI/1.0/simpleEndpoint";
    
	 
	public Vuserdata testAPIGET() throws MCSTestCaseFailedException
	{
		 Vuserdata vuserdata=null;
		
		 try {
			   
			    vuserdata = new Vuserdata();
			    vuserdata.setTestcase("testAPIGET");
			    vuserdata.setVuser(vuser);
			    long starttime = System.currentTimeMillis();
				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpGet getRequest = new HttpGet(URL);
				getRequest.addHeader("accept", "application/json");
				getRequest.addHeader("Backend-Token", "DummyMBE/1.0");
			    
				
				HttpResponse response = httpClient.execute(getRequest);
				
				long rtMs=System.currentTimeMillis() - starttime;					
				vuserdata.setRtMs(rtMs);
				int status_code = response.getStatusLine().getStatusCode();
				vuserdata.setStatusCode(status_code);
				
				System.out.println("response code="+status_code);
				
				if (response.getStatusLine().getStatusCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
					   + response.getStatusLine().getStatusCode());
				}
		 
				BufferedReader br = new BufferedReader(
		                         new InputStreamReader((response.getEntity().getContent())));
		 
				String output;
				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {
					System.out.println(output);
				}
		 
				httpClient.getConnectionManager().shutdown();
		 
			  } catch (ClientProtocolException e) {
		 
				e.printStackTrace();
				throw new MCSTestCaseFailedException("testAPIGET failed", e);
		 
			  } catch (IOException e) {
		 
				e.printStackTrace();
				throw new MCSTestCaseFailedException("testAPIGET failed", e);
			  }
		 return vuserdata;
	}
	
	public Vuserdata testAPIPOST() throws MCSTestCaseFailedException
	{
		Vuserdata vuserdata=null;
		
		try {
			  vuserdata = new Vuserdata();
			  vuserdata.setVuser(vuser);
			  vuserdata.setTestcase("testAPIPOST");
			  vuserdata.setPass('N');
			  long starttime = System.currentTimeMillis(); 
			  DefaultHttpClient httpClient = new DefaultHttpClient();
			  HttpPost postRequest = new HttpPost(URL);
			  postRequest.addHeader("Backend-Token", "DummyMBE/1.0");
			//StringEntity input = new StringEntity("{\"qty\":100,\"name\":\"iPad 4\"}");
			//input.setContentType("application/json");			
			//postRequest.setEntity(input);
	 
			HttpResponse response = httpClient.execute(postRequest);
			
			long rtMs=System.currentTimeMillis() - starttime;					
			vuserdata.setRtMs(rtMs);
			int status_code = response.getStatusLine().getStatusCode();
			if ( status_code == 200 ) vuserdata.setPass('Y');
			vuserdata.setStatusCode(status_code);
			
	        System.out.println("response code="+status_code);
	        
	        
			
	        /*
			if (response.getStatusLine().getStatusCode() != 201) {
				throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatusLine().getStatusCode());
			}
	        */
	        
			BufferedReader br = new BufferedReader(
	                        new InputStreamReader((response.getEntity().getContent())));
	 
			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
	 
			httpClient.getConnectionManager().shutdown();
	 
		  } catch (MalformedURLException e) {
	 
			e.printStackTrace();
			throw new MCSTestCaseFailedException("testAPIPOST failed", e);
		  } catch (IOException e) {
	 
			e.printStackTrace();
			throw new MCSTestCaseFailedException("testAPIPOST failed", e);
		  }
		return vuserdata;
	}
	
	public Vuserdata testAPIPUT() throws MCSTestCaseFailedException
	{
		Vuserdata vuserdata=null;
		try {
			  vuserdata = new Vuserdata();
			  vuserdata.setVuser(vuser);
			  vuserdata.setTestcase("testAPIPUT");
			  vuserdata.setPass('N');
			  long starttime = System.currentTimeMillis(); 
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPut putRequest = new HttpPut(URL);
			//StringEntity input = new StringEntity("{\"qty\":100,\"name\":\"iPad 4\"}");
			//input.setContentType("application/json");
			//putRequest.setEntity(input);
	 
			HttpResponse response = httpClient.execute(putRequest);
			
			long rtMs=System.currentTimeMillis() - starttime;					
			vuserdata.setRtMs(rtMs);
			int status_code = response.getStatusLine().getStatusCode();
			vuserdata.setStatusCode(status_code);
			if ( status_code == 200 ) vuserdata.setPass('Y');
			
			System.out.println("response code="+status_code);
	 
			/*
			if (response.getStatusLine().getStatusCode() != 201) {
				throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatusLine().getStatusCode());
			}
	        */
			BufferedReader br = new BufferedReader(
	                        new InputStreamReader((response.getEntity().getContent())));
	 
			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
	 
			httpClient.getConnectionManager().shutdown();
	 
		  } catch (MalformedURLException e) {
	 
			e.printStackTrace();
			throw new MCSTestCaseFailedException("testAPIPUT failed", e);
	 
		  } catch (IOException e) {
	 
			e.printStackTrace();
			throw new MCSTestCaseFailedException("testAPIPUT failed", e);
	 
		  }
		return vuserdata;
	}
	
	public Vuserdata testAPIDELETE() throws MCSTestCaseFailedException
	{
		Vuserdata vuserdata=null;
		try {
			  vuserdata = new Vuserdata();
			  vuserdata.setVuser(vuser);
			  vuserdata.setTestcase("testAPIDELETE");
			  vuserdata.setPass('N');
			  long starttime = System.currentTimeMillis(); 
			  DefaultHttpClient httpClient = new DefaultHttpClient();
			  HttpDelete deleteRequest = new HttpDelete(URL);
			// Entity not allowed for DELETE request
			//StringEntity input = new StringEntity("{\"qty\":100,\"name\":\"iPad 4\"}");
			//input.setContentType("application/json");
			//deleteRequest.setEntity(input);
			
			deleteRequest.setHeader("Accept", "application/json");	 
			HttpResponse response = httpClient.execute(deleteRequest);
			
			long rtMs=System.currentTimeMillis() - starttime;					
			vuserdata.setRtMs(rtMs);
			int status_code = response.getStatusLine().getStatusCode();
			if ( status_code == 200 ) vuserdata.setPass('Y');
			vuserdata.setStatusCode(status_code);
	 
			System.out.println("response code="+response.getStatusLine().getStatusCode());
			
			/*
			if (response.getStatusLine().getStatusCode() != 201) {
				throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatusLine().getStatusCode());
			}
	        */
			BufferedReader br = new BufferedReader(
	                        new InputStreamReader((response.getEntity().getContent())));
	 
			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
	 
			httpClient.getConnectionManager().shutdown();
	 
		  } catch (MalformedURLException e) {
	 
			e.printStackTrace();
			throw new MCSTestCaseFailedException("testAPIDELETE failed", e);
	 
		  } catch (IOException e) {
	 
			e.printStackTrace();
			throw new MCSTestCaseFailedException("testAPIDELETE failed", e);
	 
		  }
		
		return vuserdata;
	}
	
	
	public Set<Vuserdata> run() throws MCSTestCaseFailedException
	{
		Set<Vuserdata> vuserdatas = new HashSet<Vuserdata>();
		try {
		vuserdatas.add(testAPIGET());
	    Thread.sleep(loadScenario.getThinktime());		
		vuserdatas.add(testAPIPOST());
		Thread.sleep(loadScenario.getThinktime());	
		vuserdatas.add(testAPIPUT());
		Thread.sleep(loadScenario.getThinktime());	
		vuserdatas.add(testAPIDELETE());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return vuserdatas;
		
	}
	public static void main(String[] args) {
		MCSClient_CC mcsclient = new MCSClient_CC();		
		try {
			mcsclient.run();
		} catch (MCSTestCaseFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @return the loadScenario
	 */
	public LoadScenario getLoadScenario() {
		return loadScenario;
	}

	/**
	 * @param loadScenario the loadScenario to set
	 */
	public void setLoadScenario(LoadScenario loadScenario) {
		this.loadScenario = loadScenario;
	}

	/**
	 * @return the vuser
	 */
	public Vuser getVuser() {
		return vuser;
	}

	/**
	 * @param vuser the vuser to set
	 */
	public void setVuser(Vuser vuser) {
		this.vuser = vuser;
	}
	
	
 
}