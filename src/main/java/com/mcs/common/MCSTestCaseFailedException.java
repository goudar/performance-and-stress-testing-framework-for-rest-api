package com.mcs.common;

public class MCSTestCaseFailedException extends Exception {
	   private String message;

	    public MCSTestCaseFailedException(){
	    	super();
	    }
	 	public MCSTestCaseFailedException(String message){
	 		super(message);
	 	}
	 	public MCSTestCaseFailedException(String message, Throwable cause){
	 		super(message,cause);
	 	}
	 	public MCSTestCaseFailedException(Throwable cause){
	 		super(cause);
	 		
	 	}
		
		public String getMessage() {
			return message;
		}
	
		public void setMessage(String message) {
			this.message = message;
		}
}
