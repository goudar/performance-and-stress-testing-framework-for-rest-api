package com.mcs.common;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import model.*;

public class Controller {
	private Run run;
	private LoadScenario loadScenario;

	private Vuser vusers[];
	private VuserThread vuserThreads[];
	private Thread thread_vusers[];
	private HibUtil hibUtil;
	
	
	
	public void setRun(Run run)
	{
		this.run=run;
	}
	public Run getRun()
	{
		return run;
	}
	
	
	public LoadScenario getLoadScenario() {
		return loadScenario;
	}
	
	public void setLoadScenario(LoadScenario loadScenario) {
		this.loadScenario = loadScenario;
	}
	
	
	public void initThreads(int users)
	{
	     vuserThreads = new VuserThread[users];
	     thread_vusers = new Thread[users];
		 for(int i=0;i<users;i++)
		 {		 		
			vuserThreads[i] = new VuserThread();
			vuserThreads[i].setVuser(vusers[i]);			
			vuserThreads[i].setLoadScenario(loadScenario);
			vuserThreads[i].hibUtil=hibUtil;
			thread_vusers[i] = new Thread(vuserThreads[i],vusers[i].getVusername());
		 }
		
	}

	public boolean initDBData(int users, String name)
	{
	    vusers = new Vuser[users];
		run.setName(name);	
		for(int i=0;i<users;i++)
		{
   	      vusers[i] = new Vuser();
		  vusers[i].setRun(run);
		  run.getVusers().add(vusers[i]);		
		}
		
		SessionFactory session_factory=null;
		Session session = hibUtil.createSession();
		try{
		     
		    
		     session.beginTransaction();
		     session.save(run);
		     for(int i=0; i < users;i++)
			 {	
				vusers[i].setVusername("vuser"+vusers[i].getVuserid());
			 }

		     session.saveOrUpdate(run);
		     session.getTransaction().commit();
		     session.close();
		}catch(Exception ex){
			  ex.printStackTrace();	
			  return false;
		}    
			
		return true;
		
		
		
	}
	
	
	
	public void start(int users)
	{
		 for(int i=0;i<users;i++)
		 {
			thread_vusers[i].start(); 
		 }
		 for(int i=0;i<users;i++)
		 {
			try {
				thread_vusers[i].join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
	}
	
	public static void main(String args[])
	{
		
		
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        Controller controller = (Controller)context.getBean("controllerBean");
        LoadScenario ls = controller.getLoadScenario();
        System.out.println(ls.getUsers());
        controller.initDBData(ls.getUsers(), "run2");
        controller.initThreads(ls.getUsers());
        long starttime=System.currentTimeMillis();
        controller.start(ls.getUsers());
        long total_time=System.currentTimeMillis() - starttime;
        System.out.println("Time Take to execute="+total_time);
	}
	/**
	 * @return the hibUtil
	 */
	public HibUtil getHibUtil() {
		return hibUtil;
	}
	/**
	 * @param hibUtil the hibUtil to set
	 */
	public void setHibUtil(HibUtil hibUtil) {
		this.hibUtil = hibUtil;
	}
	

}
