package com.mcs.common;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;

import model.Vuser;
import model.Vuserdata;

public class StoreData {
	private Vuser vuser;
	private Vuserdata vuserdata;
	private Session session;
	private HibUtil hibUtil;
	private String logFilePath;
	private BufferedWriter bw;
	boolean done=false;
	
	public void init() throws MCSStoreDBException, MCSStoreFileException
	{
		initDB();
		initFile();
	}

	public synchronized void  addVuserdataToVuser(Set<Vuserdata> vuserdatas)
	{
		vuser.setVuserdatas(vuserdatas);
		notifyAll();
	}
	
	public synchronized void store() throws MCSStoreDBException, MCSStoreFileException
	{
		while(!done)
		{
		 while ( vuser.getVuserdatas() == null) 
		  {
			  try {
				    System.out.println("vuserdata set is null and wait() is executed!");
				    wait();
				    
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  }
		 storeInFile();
		 storeInDb();
		}
	}
	
	
	public synchronized void storeInDb() throws MCSStoreDBException
	{	
	 
	  try{
		  if ( session != null)
		  {
		      if ( !session.isConnected() || !session.isOpen())
		      {
			     if (!session.isOpen() ){
		    	   System.out.println("session closed"); 
		          }
		       
		          if ( session.isConnected()){
		    	   System.out.println("session not connected"); 		    	  
		          }
			
			      try{
			          session.close();
			         }catch(Exception ex)
			         {
			  	
			         }
			      session = hibUtil.createSession();
		       }
		 }else{
			 session = hibUtil.createSession();
		 }
  	      session.beginTransaction();
		  session.saveOrUpdate(vuser);
		  session.getTransaction().commit();		
		  vuser.getVuserdatas().clear();
		  vuser.setVuserdatas(null);
		  
		}catch(Exception ex)
		{
			throw new MCSStoreDBException("Hibernate unable to store data to DB",ex); 
		}
	 }
	
	public void initDB() throws MCSStoreDBException
	{
		try{
		     session = hibUtil.createSession();
	       }catch(Exception ex)
	      {
		      throw new MCSStoreDBException("Hibernate unable to store data to DB",ex); 
	      }	
	}
	
	public void initFile() throws MCSStoreFileException
	{
		try {
			 			
			  File file = new File(logFilePath+vuser.getVusername());
			  // if file doesnt exists, then create it
			  if (!file.exists()) {
				file.createNewFile();
			  }
 
			  FileWriter fw = new FileWriter(file.getAbsoluteFile());
			  bw = new BufferedWriter(fw);					

		} catch (IOException e) {
			e.printStackTrace();
			throw new MCSStoreFileException("Unable to create log file to write data",e);
		}
	}
	public synchronized void storeInFile() throws MCSStoreFileException
	{ 
		
		try {	 
			   String content = "This is the content to write into file";
			   for(Vuserdata i:vuser.getVuserdatas()){
				   content=i.getTestcase()+":"+i.getRtMs()+":"+i.getPass()+":"+i.getStatusCode();
				   bw.write(content);
				   bw.newLine();
				}				
				bw.write(content);
				bw.newLine();
				bw.flush();
			} catch (IOException ex) {
				ex.printStackTrace();
				throw new MCSStoreFileException("Unable to create log file to write data",ex);
			}
		}

   public void closeFile()  
   {
	   try {
		    bw.close();
	   } catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	   }
   }
   
   public void closeSession()
   {
	   if ( session != null){
	     session.close();
	   }
	   
   }
	/**
	 * @return the vuser
	 */
	public Vuser getVuser() {
		return vuser;
	}

	/**
	 * @param vuser the vuser to set
	 */
	public void setVuser(Vuser vuser) {
		this.vuser = vuser;
	}

	/**
	 * @return the vuserdata
	 */
	public Vuserdata getVuserdata() {
		return vuserdata;
	}

	/**
	 * @param vuserdata the vuserdata to set
	 */
	public void setVuserdata(Vuserdata vuserdata) {
		this.vuserdata = vuserdata;
	}

	/**
	 * @return the session
	 */
	public Session getSession() {
		return session;
	}

	/**
	 * @param session the session to set
	 */
	public void setSession(Session session) {
		this.session = session;
	}

	/**
	 * @return the hibUtil
	 */
	public HibUtil getHibUtil() {
		return hibUtil;
	}

	/**
	 * @param hibUtil the hibUtil to set
	 */
	public void setHibUtil(HibUtil hibUtil) {
		this.hibUtil = hibUtil;
	}

	/**
	 * @return the logFilePath
	 */
	public String getLogFilePath() {
		return logFilePath;
	}

	/**
	 * @param logFilePath the logFilePath to set
	 */
	public void setLogFilePath(String logFilePath) {
		this.logFilePath = logFilePath;
	}

}
