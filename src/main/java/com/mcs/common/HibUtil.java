package com.mcs.common;

import model.Vuser;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class HibUtil {
	
	private SessionFactory session_factory;
	

	public Session createSession()
	{
		return session_factory.openSession();
	}
	public HibUtil()
	{
		init();
		System.out.println("SessionFactory is initialized");
		
	}
	
	public void init()
	{
		try{
		       session_factory =  new AnnotationConfiguration().configure().buildSessionFactory();
		       
	        }catch(Exception ex){
		         ex.printStackTrace();
		         throw new ExceptionInInitializerError(ex);
	           }  
	}
	
}
