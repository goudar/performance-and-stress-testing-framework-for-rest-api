package com.mcs.common;

public class MCSStoreDBException extends Exception {
	   private String message;

	    public MCSStoreDBException(){
	    	super();
	    }
	 	public MCSStoreDBException(String message){
	 		super(message);
	 	}
	 	public MCSStoreDBException(String message, Throwable cause){
	 		super(message,cause);
	 	}
	 	public MCSStoreDBException(Throwable cause){
	 		super(cause);
	 		
	 	}
		
		public String getMessage() {
			return message;
		}
	
		public void setMessage(String message) {
			this.message = message;
		}
}
