package com.mcs.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class LoadScenario {
	private int users;
	private int thinktime;
	private int iterations;
	private String name;;
	private String logFilePath;
	/**
	 * @return the users
	 */
	LoadScenario()
	{
		//init();
	}
	
	public void init() 
	{
		System.out.println("Running LoadScenario init()");
		Properties prop = new Properties();
    	InputStream inputstream = null;
        
        try{
        	String filename = "config.properties";
            inputstream = LoadScenario.class.getClassLoader().getResourceAsStream(filename);
            
            if ( inputstream == null)
            {
            	System.out.println("Unable to load the config.properties");
            	return;
            }
            prop.load(inputstream);
            setThinktime(Integer.parseInt(prop.getProperty("thinktime")));
            System.out.println(prop.getProperty("iterations"));
            setIterations(Integer.parseInt(prop.getProperty("iterations")));
            setUsers(Integer.parseInt(prop.getProperty("iterations")));
     
        }catch(Exception ex)
        {
        	ex.printStackTrace();
        }finally{
        	     if ( inputstream != null ){
        	    	 try {
						inputstream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
        	     }
        }
       
 
	}
	public int getUsers() {
		return users;
	}
	/**
	 * @param users the users to set
	 */
	public void setUsers(int users) {
		this.users = users;
	}
	/**
	 * @return the thinktime
	 */
	public int getThinktime() {
		return thinktime;
	}
	/**
	 * @param thinktime the thinktime to set
	 */
	public void setThinktime(int thinktime) {
		this.thinktime = thinktime;
	}
	/**
	 * @return the iterations
	 */
	public int getIterations() {
		return iterations;
	}
	/**
	 * @param iterations the iterations to set
	 */
	public void setIterations(int iterations) {
		this.iterations = iterations;
	}
	
	public void nonspring()
	{
		LoadScenario ls = new LoadScenario();
		ls.init();
		
		
	}
	public static void main(String args[])
	{
		
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        //StoreData storeData = (StoreData)context.getBean("storeData");
        //storeData.init(1, "run3");
		LoadScenario loadScenario = (LoadScenario)context.getBean("loadScenarioBean");
		System.out.println("iterations=>"+loadScenario.getIterations());
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the logFilePath
	 */
	public String getLogFilePath() {
		return logFilePath;
	}

	/**
	 * @param logFilePath the logFilePath to set
	 */
	public void setLogFilePath(String logFilePath) {
		this.logFilePath = logFilePath;
	}

}
