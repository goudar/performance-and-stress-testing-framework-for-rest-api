package com.mcs.common;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;

import model.Vuser;
import model.Vuserdata;


public class VuserThread implements Runnable{
	   private LoadScenario loadScenario;
       private Vuser vuser;
       StoreData storeData;
       HibUtil hibUtil;
       
     
     
       
		public void run() {  
			vuser.setVuserdatas(null);
			storeData = new StoreData();		    
			storeData.setHibUtil(hibUtil);
			storeData.setVuser(vuser);
			storeData.setLogFilePath(loadScenario.getLogFilePath());
			MCSClient_CC mcs_client = new MCSClient_CC();
			mcs_client.setLoadScenario(loadScenario);
			mcs_client.setVuser(vuser);

		    try {
				storeData.init();
			} catch (MCSStoreDBException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (MCSStoreFileException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    long count=0;
			DBHandlerThread dbthread = new DBHandlerThread();
		    dbthread.storeData=storeData;				    				   
		    Thread th = new Thread(dbthread, "dbthread_v"+vuser.getVuserid()+"_"+count);			 
		    th.start();					
			System.out.println("Thread="+Thread.currentThread().getName()+" is running");
			Set<Vuserdata> vuserdatas = new HashSet<Vuserdata>(0);
			
			
			while (count < loadScenario.getIterations()){
				count++;
				System.out.println("vuserid="+vuser.getVuserid()+" interation="+count);
				Vuserdata vuserdata = new Vuserdata();
				vuserdata.setTestcase("TestAPI");
				vuserdata.setVuser(vuser);				
				vuserdatas.add(vuserdata);
				long starttime = System.currentTimeMillis();
				
				try {
					 vuserdatas.addAll(mcs_client.run());
					 vuserdata.setPass('Y');
				} catch (MCSTestCaseFailedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					vuserdata.setPass('N');
				}
				long rt=System.currentTimeMillis() - starttime;
				System.out.println("TEST TIME:"+rt);
				 vuserdata.setRtMs(rt);
				 try {						
				      Thread.sleep(loadScenario.getThinktime());
				    } catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
					

				 
				 if ( vuserdatas.size() == 100 )
				 {
					 storeData.addVuserdataToVuser(vuserdatas);
					 vuserdatas = new HashSet<Vuserdata>(0);
				 }
				 
			}
			storeData.addVuserdataToVuser(vuserdatas);
			 while ( vuser.getVuserdatas() != null ) 
			 {
				 try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			 }
					 
		    if ( th != null)
		    {
		    	if(th.isAlive() )
		    	{
		    	  th.destroy();
		    	}
		    }		    
				storeData.closeFile();
				storeData.getSession().close();
		    	
		}
			
		
		
		

		/**
		 * @return the loadScenario
		 */
		public LoadScenario getLoadScenario() {
			return loadScenario;
		}

		/**
		 * @param loadScenario the loadScenario to set
		 */
		public void setLoadScenario(LoadScenario loadScenario) {
			this.loadScenario = loadScenario;
		}

		/**
		 * @return the vuser
		 */
		public Vuser getVuser() {
			return vuser;
		}

		/**
		 * @param vuser the vuser to set
		 */
		public void setVuser(Vuser vuser) {
			this.vuser = vuser;
		}

		/**
		 * @return the vuserdata
		 */
	
		
	}
	

